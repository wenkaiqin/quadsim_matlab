%#ok<*UNRCH>

% Runs single-axis step response, either translational or rotational and
% finds the rise time/overshoot.
%
% Usage: change mode according to desired axis and type, adjust step sizes,
% then run.

clear, clc
if strcmp(getenv('USER'), 'wenkaiqin')
    path(pathdef)
    addpath(genpath(pwd))
end

% Step sizes.
trans_step =           0; % m
rot_step   = deg2rad(70); % rad

% Type of step response.
mode = 'transX';
% mode = 'transY';
% mode = 'transZ';
% mode =   'rotX';
% mode =   'rotY';
% mode =   'rotZ';

%% Run simulation.

% Quadrotor parameters and constants.
quadParamsScript;
constantsScript;
sensorParamsScript
P.quadParams   = quadParams;
P.constants    = constants;
P.sensorParams = sensorParams;

% Simulation parameters.
Tsim = 5;
delt = 0.001;
N    = floor(Tsim/delt)+1;

S.oversampFact = 10;
S.distMat      = zeros(N-1,3);
S.rXIMat = [0,0,0; 0,0,0.7];

% Tune with deviations from hovering at the origin with RBI = identity. Set
% reference trajectory to remain still at the origin, then set initial
% conditions to be displaced.
R.tVec         = (0:N-1)'*delt;

R.rIstar = zeros(length(R.tVec),3);
R.vIstar = zeros(length(R.tVec),3);
R.aIstar = zeros(length(R.tVec),3);

R.xIstar = zeros(length(R.tVec),3);
R.xIstar(:,1) = 1;

% Set initial conditions.
switch mode

    case 'transX'
        S.state0.r      = [trans_step 0 0]';
        S.state0.e      = [         0 0 0]';
        S.state0.v      = [         0 0 0]';
        S.state0.omegaB = zeros(3,1);

    case 'transY'
        S.state0.r      = [ 0 trans_step 0]';
        S.state0.e      = [ 0          0 0]';
        S.state0.v      = [ 0          0 0]';
        S.state0.omegaB = zeros(3,1);

    case 'transZ'
        S.state0.r      = [ 0 0 trans_step]';
        S.state0.e      = [ 0 0          0]';
        S.state0.v      = [ 0 0          0]';
        S.state0.omegaB = zeros(3,1);

    case 'rotX'
        S.state0.r      = [       0 0 0]';
        S.state0.e      = [rot_step 0 0]';
        S.state0.v      = [       0 0 0]';
        S.state0.omegaB = zeros(3,1);

    case 'rotY'
        S.state0.r      = [ 0        0 0]';
        S.state0.e      = [ 0 rot_step 0]';
        S.state0.v      = [ 0        0 0]';
        S.state0.omegaB = zeros(3,1);

    case 'rotZ'
        S.state0.r      = [ 0 0        0]';
        S.state0.e      = [ 0 0 rot_step]';
        S.state0.v      = [ 0 0        0]';
        S.state0.omegaB = zeros(3,1);
end

% Simulate.
% Q = simulateQuadrotorControl(R,S,P);
Q = simulateQuadrotorEstimationAndControl(R,S,P);
% visualizeQuad(S2, R);

%% Plot and find step responses.
S2.tVec          = Q.tVec;
S2.rMat          = Q.state.rMat;
S2.eMat          = Q.state.eMat;
S2.plotFrequency = 30;
S2.makeGifFlag   = true;
S2.gifFileName   = 'testGif.gif';
S2.bounds        = 5*[-1 1 -1 1 -1 1];

plotQuadDynamics(Q);

if strcmp(mode(1:end-1),'trans')

    % Select position plot.
    figure(2)
    if     mode(end) == 'X'
        subplot(311)
        step_metrics_t(Q,1,trans_step);

    elseif mode(end) == 'Y'
        subplot(312)
        step_metrics_t(Q,2,trans_step);

    elseif mode(end) == 'Z'
        subplot(313)
        step_metrics_t(Q,3,trans_step);

    end

elseif strcmp(mode(1:end-1),'rot')

    % Select position plot.
    figure(3)
    if     mode(end) == 'X'
        subplot(311)
        step_metrics_r(Q,1,rot_step);
    elseif mode(end) == 'Y'
        subplot(312)
        step_metrics_r(Q,2,rot_step);
    elseif mode(end) == 'Z'
        subplot(313)
        step_metrics_r(Q,3,rot_step);
    end

end

%% Helpers.
function [rise_time, overshoot] = step_metrics_t(data, axis, trans_step)

cross_idx = find(data.state.rMat(:,axis)<0.1*trans_step,1);
rise_time = data.tVec(cross_idx);
rise_label = sprintf('90%% Rise Time = %.2f s', rise_time);
xline(rise_time, 'r-.', rise_label, 'LabelVerticalAlignment','top', ...
                                    'LabelOrientation','horizontal')

extreme   = min(data.state.rMat(:,axis));
overshoot = -extreme/trans_step * 100;
overshoot_label = sprintf('Overshoot = %.2f%%%', overshoot);
yline(extreme, 'r-.', overshoot_label, 'LabelHorizontalAlignment', 'right', ...
                                       'LabelVerticalAlignment'  , 'top')

end

function [rise_time, overshoot] = step_metrics_r(data, axis, rot_step)

cross_idx = find(data.state.eMat(:,axis)<0.1*rot_step,1);
rise_time = data.tVec(cross_idx);
rise_label = sprintf('90%% Rise Time = %.2f s', rise_time);
xline(rise_time, 'r-.', rise_label, 'LabelVerticalAlignment','top', ...
                                    'LabelOrientation','horizontal')

extreme   = min(data.state.eMat(:,axis));
overshoot = -extreme/rot_step * 100;
overshoot_label = sprintf('Overshoot = %.2f%%%', overshoot);
yline(rad2deg(extreme), 'r-.', overshoot_label, 'LabelHorizontalAlignment', 'right', ...
                                                'LabelVerticalAlignment'  , 'top')

end
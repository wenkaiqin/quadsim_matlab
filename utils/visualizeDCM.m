% Visualize DCM on a plot.
function visualizeDCM(R)

axes_old = eye(3);
axes_new = R'*axes_old;     % Each *column* is the new axis.

hold on, grid on
quiver3(   zeros(1,3),    zeros(1,3),    zeros(1,3), ...
              [1 0 0],       [0 1 0],       [0 0 1], "off")
quiver3(   zeros(1,3),    zeros(1,3),    zeros(1,3), ...
        axes_new(1,:), axes_new(2,:), axes_new(3,:), "off", "--")
view(3)

legend('Old Axes','New Axes', 'Location','northwest')
axis equal

% Old axes labels.
text(axes_old(1,1), axes_old(2,1), axes_old(3,1), 'x', 'HorizontalAlignment', 'left')
text(axes_old(1,2), axes_old(2,2), axes_old(3,2), 'y', 'HorizontalAlignment', 'left')
text(axes_old(1,3), axes_old(2,3), axes_old(3,3), 'z', 'HorizontalAlignment', 'left')

% New axes labels.
text(axes_new(1,1), axes_new(2,1), axes_new(3,1), 'X', 'HorizontalAlignment', 'right')
text(axes_new(1,2), axes_new(2,2), axes_new(3,2), 'Y', 'HorizontalAlignment', 'right')
text(axes_new(1,3), axes_new(2,3), axes_new(3,3), 'Z', 'HorizontalAlignment', 'right')

axis(sqrt(2)*[-1 1 -1 1 -1 1])

end
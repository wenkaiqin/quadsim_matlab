function [R_BW] = euler2dcm(e)
% euler2dcm : Converts Euler angles phi = e(1), theta = e(2), and psi = e(3)
%             (in radians) into a direction cosine matrix for a 3-1-2 rotation.
%
% Let the world (W) and body (B) reference frames be initially aligned.  In a
% 3-1-2 order, rotate B away from W by angles psi (yaw, about the body Z
% axis), phi (roll, about the body X axis), and theta (pitch, about the body Y
% axis).  R_BW can then be used to cast a vector expressed in W coordinates as
% a vector in B coordinates: vB = R_BW * vW
%
% INPUTS
%
% e ---------- 3-by-1 vector containing the Euler angles in radians: phi =
%              e(1), theta = e(2), and psi = e(3)
%
%
% OUTPUTS
%
% R_BW ------- 3-by-3 direction cosine matrix 
% 
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+  

eps = 1e-4;

roll  = e(1);
pitch = e(2);
yaw   = e(3);

if abs(abs(roll)-pi/2)<eps
    warning('Given Euler angles (312) result in singular rotation matrix.')
end

R_BW = rotationMatrix([0 1 0], pitch) ...
     * rotationMatrix([1 0 0], roll ) ...
     * rotationMatrix([0 0 1], yaw  );

end
% Makes a set of 5 plots to visualize quadcopter movement.
% Usage: 
% Q = simulateQuadrotorControl(R,S,P);
% plotQuadDynamics(Q);

% TODO: plot reference trajectory as well
function plotQuadDynamics(data, reference, linestyle, togglelimit)

if nargin < 2 || isempty(reference)
    plotref = false;
else
    plotref = true;
end

if nargin < 3 || isempty(linestyle)
    linestyle = '-';
end

if nargin < 4 || isempty(togglelimit)
    togglelimit = true;
end

% 3D position.
figure(1), clf
hold on, grid on
% Start and end points.
scatter3(data.state.rMat(  1,1),data.state.rMat(  1,2),data.state.rMat(  1,3),'ko')
scatter3(data.state.rMat(end,1),data.state.rMat(end,2),data.state.rMat(end,3),'kx')
legstr = {'Start','End'};
% Trajectory (and reference, if available).
if plotref
    plot3(reference.rIstar(:,1),reference.rIstar(:,2),reference.rIstar(:,3), 'r--')
    legstr{end+1} = 'Reference';
end
plot3(data.state.rMat(:,1), data.state.rMat(:,2), data.state.rMat(:,3), 'k-');
% Reset axis labels to something cubic.
curr_lims = [xlim; ylim; zlim];
axis equal
ctr_lims  = mean(curr_lims,2);
rng_lims  = curr_lims(:,2)-curr_lims(:,1);
new_lims  = ctr_lims+max(rng_lims(:))/2*[-1 1];
axis(reshape(new_lims',[1 6]))
view(3)
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
sgtitle('3D Trajectory');
% Legend.
legend(legstr,'Location','best')

% XYZ position.
figure(2), clf
ylabels={'X (m)','Y (m)','Z (m)'};
for dim=1:3
    subplot(3,1,dim)
    hold on, grid on
    plot(data.tVec, data.state.rMat(:,dim), linestyle)
    xlabel('t (s)')
    ylabel(ylabels{dim})
    if togglelimit
        lim_yplot()
    end
end
sgtitle('Position')

% Euler angles.
figure(3), clf
ylabels={'Roll (\circ)','Pitch (\circ)','Yaw (\circ)'};
for dim=1:3
    subplot(3,1,dim)
    hold on, grid on
    plot(data.tVec, rad2deg(data.state.eMat(:,dim)), linestyle)
    ylabel(ylabels{dim})
    if togglelimit
        lim_yplot()
    end
end
xlabel('Time (s)')
sgtitle('Euler Angles')

% Velocities.
figure(4), clf
ylabels={'v_X (m/s)','v_Y (m/s)','v_Z (m/s)'};
for dim=1:3
    subplot(3,1,dim)
    hold on, grid on
    plot(data.tVec, data.state.vMat(:,dim), linestyle) 
    ylabel(ylabels{dim})
    if togglelimit
        lim_yplot()
    end
end
xlabel('Time (s)')
sgtitle('Velocities')

% Body rates.
figure(5), clf
ylabels={'Roll Rate (\circ/s)','Pitch Rate (\circ/s)','Yaw Rate (\circ/s)'};
for dim=1:3
    subplot(3,1,dim)
    hold on, grid on
    plot(data.tVec, rad2deg(data.state.omegaBMat(:,dim)), linestyle)
    ylabel(ylabels{dim})
    if togglelimit
        lim_yplot()
    end
end
xlabel('Time (s)')
sgtitle('Body Rates')

% Propeller rates.
% figure(6), clf
% ylabels={'Roll Rate (\circ)','Pitch Rate (\circ)','Yaw Rate (\circ)'};
% for dim=1:3
%     subplot(3,1,dim)
%     hold on, grid on
%     plot(P.tVec, P.state.omegaBMat(:,dim), linestyle)
%     ylabel(ylabels{dim})
%     if togglelimit
%         lim_yplot()
%     end
% end
% xlabel('Time (s)')
% sgtitle('Body Rates')

set_plot_locs()

end

function lim_yplot(min_width)

if nargin<1
    min_width = 0.01;
end

curr_lims = ylim;

if(max(curr_lims)-min(curr_lims)) < min_width
    ylim([mean(curr_lims)-min_width/2 mean(curr_lims)+min_width/2])
end

end

function set_plot_locs()

figure(1)
set(gcf, 'Units', 'normalized', 'Position', [0.0098    0.5175    0.3241    0.3760]);
figure(2)
set(gcf, 'Units', 'normalized', 'Position', [0.3345    0.5175    0.3241    0.3760]);
figure(3)
set(gcf, 'Units', 'normalized', 'Position', [0.6591    0.5175    0.3241    0.3760]);
figure(4)
set(gcf, 'Units', 'normalized', 'Position', [0.0098    0.0698    0.3241    0.3760]);
figure(5)
set(gcf, 'Units', 'normalized', 'Position', [0.3345    0.0698    0.3241    0.3760]);
% figure(6)
% set(gcf, 'Units', 'normalized', 'Position', [0.6591    0.0698    0.3241    0.3760]);

end
function [rx] = hdCameraSimulator(rXI,S,P)
% hdCameraSimulator : Simulates feature location measurements from the
%                     quad's high-definition camera. 
%
%
% INPUTS
%
% rXI -------- 3x1 location of a feature point expressed in I in meters.
%
% S ---------- Structure with the following elements:
%
%        statek = State of the quad at tk, expressed as a structure with the
%                 following elements:
%                   
%                  rI = 3x1 position of CM in the I frame, in meters
% 
%                 RBI = 3x3 direction cosine matrix indicating the
%                       attitude of B frame wrt I frame
%
% P ---------- Structure with the following elements:
%
%  sensorParams = Structure containing all relevant parameters for the
%                 quad's sensors, as defined in sensorParamsScript.m 
%
% OUTPUTS
%
% rx --------- 2x1 measured position of the feature point projection on the
%              camera's image plane, in pixels.  If the feature point is not
%              visible to the camera (the ray from the feature to the camera
%              center never intersects the image plane, or the feature is
%              behind the camera), then rx is an empty matrix.
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+  

% Camera's intrinsic matrix is identity because we directly take Xc, Yc, Zc
% in camera frame. Compose RCI from static body-to-camera body rotation and
% dynamic state-dependent RBI. Then, take homogeneous point in camera space
% as:
%
%    x = PX = K*[RCI,t]*X
%
% Where x is the homogenous camera-frame coordinate, K is the camera
% intrinsic matrix, RCI is the ENU->camera rotation matrix, t is the
% translational ENU-camera displacement in camera frame, and X is rXI point
% under consideration appended with 1. All units in meters.

RBI = S.statek.RBI;
RCB = P.sensorParams.RCB;

K = P.sensorParams.K;
RCI = RCB * RBI;
t = -RCB*P.sensorParams.rocB-RCI*S.statek.rI;

x = K * [RCI, t] * [rXI; 1];

% "De-homogenize" x into the camera plane position in the camera. Filter
% based on whether or not we are within or behind the frame. Still meters.
xc = [x(1)/x(3)
      x(2)/x(3)];

if(any(abs(xc)>P.sensorParams.imagePlaneSize'/2) || x(3)<0)
    rx = [];
    return
end

% Convert to pixels, and add noise.
wc = mvnrnd(zeros(2,1), P.sensorParams.Rc)';
rx = (1/P.sensorParams.pixelSize)*xc+wc;

end
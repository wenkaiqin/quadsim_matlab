function [rpGtilde,rbGtilde] = gnssMeasSimulator(S,P)
% gnssMeasSimulator : Simulates GNSS measurements for quad.
%
%
% INPUTS
%
% S ---------- Structure with the following elements:
%
%        statek = State of the quad at tk, expressed as a structure with the
%                 following elements:
%                   
%                  rI = 3x1 position of CM in the I frame, in meters
% 
%                 RBI = 3x3 direction cosine matrix indicating the
%                       attitude of B frame wrt I frame
%
% P ---------- Structure with the following elements:
%
%  sensorParams = Structure containing all relevant parameters for the
%                 quad's sensors, as defined in sensorParamsScript.m 
%
%
% OUTPUTS
%
% rpGtilde --- 3x1 GNSS-measured position of the quad's primary GNSS antenna,
%              in ECEF coordinates relative to the reference antenna, in
%              meters.
%
% rbGtilde --- 3x1 GNSS-measured position of secondary GNSS antenna, in ECEF
%              coordinates relative to the primary antenna, in meters.
%              rbGtilde is constrained to satisfy norm(rbGtilde) = b, where b
%              is the known baseline distance between the two antennas.
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+
%
% For keeping track of variable names:
%
%     roF denotes position                of object relation o in frame F
%     RoF denotes noise covariance matrix of object relation o in frame F
%       w denotes noise in position, r_noisy[oF] = r[oF]+w[oF]
%     REF denotes rotation matrix from frame F to frame E
%    
%     subscript p denotes primary antenna   relative to reference station
%     subscript s denotes primary antenna   relative to reference station
%     subscript b denotes secondary antenna relative to primary antenna
%    
%       G frame is the ECEF frame
%     I/L frame is the ENU  frame, taken local to the reference station
%       B frame is the body frame
%

% Get rotation matrices based on state and reference station position. 
RIB = S.statek.RBI';
RGI = Recef2enu(P.sensorParams.r0G)';

% Form rpG, the true position of the primary antenna relative to the
% reference station in ECEF frame (G).
rpI = S.statek.rI + RIB*P.sensorParams.raB(:,1);
rpG = RGI*rpI;

% Transform the covariance in frame L to G. Add noise to rpG to get
% rpGtilde. Use mvnrnd() under the assumption noise is completely
% uncorrelated in time.
RGL = RGI; RLG = RGL';
RpG = RGL*P.sensorParams.RpL*RLG;
wpG = mvnrnd(zeros(1,3), RpG)';
rpGtilde = rpG+wpG;

% Form rbG.
rsI = S.statek.rI + RIB*P.sensorParams.raB(:,2);
rsG = RGI*rsI;
rbG = rsG-rpG;

% Form RbG, the particular covariance matrix on rbG's noise that enforces
% the constraint ‖rbG(noisy)‖ = ‖rbG(true)‖, i.e. even with noise, the
% baseline length between the two antennas remains constant. Add a small
% epsilon to the diagonals to ensure invertibility of RbG in preparation
% for Cholesky factorization.
rbG_unit = rbG/norm(rbG);
RbG = norm(rbG)^2 * P.sensorParams.sigmab^2 * (eye(3)-rbG_unit*rbG_unit');
RbG = RbG + 1e-8*eye(3);

% Add noise and renormalize to proper baseline length.
wbG = mvnrnd(zeros(3,1), RbG)';
rbGtilde = rbG + wbG;
rbGtilde = rbGtilde*norm(rbG)/norm(rbGtilde);

end
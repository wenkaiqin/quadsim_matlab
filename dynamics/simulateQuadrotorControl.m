function [Q] = simulateQuadrotorControl(R,S,P)
% simulateQuadrotorControl : Simulates closed-loop control of a quadrotor
%                            aircraft.
%
%
% INPUTS
%
% R ---------- Structure with the following elements:
%
%          tVec = Nx1 vector of uniformly-sampled time offsets from the
%                 initial time, in seconds, with tVec(1) = 0.
%
%        rIstar = Nx3 matrix of desired CM positions in the I frame, in
%                 meters.  rIstar(k,:)' is the 3x1 position at time tk =
%                 tVec(k).
%
%        vIstar = Nx3 matrix of desired CM velocities with respect to the I
%                 frame and expressed in the I frame, in meters/sec.
%                 vIstar(k,:)' is the 3x1 velocity at time tk = tVec(k).
%
%        aIstar = Nx3 matrix of desired CM accelerations with respect to the I
%                 frame and expressed in the I frame, in meters/sec^2.
%                 aIstar(k,:)' is the 3x1 acceleration at time tk =
%                 tVec(k).
%
%        xIstar = Nx3 matrix of desired body x-axis direction, expressed as a
%                 unit vector in the I frame. xIstar(k,:)' is the 3x1
%                 direction at time tk = tVec(k).
%
% S ---------- Structure with the following elements:
%
%  oversampFact = Oversampling factor. Let dtIn = R.tVec(2) - R.tVec(1). Then
%                 the output sample interval will be dtOut =
%                 dtIn/oversampFact. Must satisfy oversampFact >= 1.
%
%        state0 = State of the quad at R.tVec(1) = 0, expressed as a structure
%                 with the following elements:
%
%                   r = 3x1 position in the world frame, in meters
%
%                   e = 3x1 vector of Euler angles, in radians, indicating the
%                       attitude
%
%                   v = 3x1 velocity with respect to the world frame and
%                       expressed in the world frame, in meters per second.
%
%              omegaB = 3x1 angular rate vector expressed in the body frame,
%                       in radians per second.
%
%       distMat = (N-1)x3 matrix of disturbance forces acting on the quad's
%                 center of mass, expressed in Newtons in the world frame.
%                 distMat(k,:)' is the constant (zero-order-hold) 3x1
%                 disturbance vector acting on the quad from R.tVec(k) to
%                 R.tVec(k+1).
%
% P ---------- Structure with the following elements:
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m
%
%  sensorParams = Structure containing sensor parameters, as defined in
%                 sensorParamsScript.m
%
%
% OUTPUTS
%
% Q ---------- Structure with the following elements:
%
%          tVec = Mx1 vector of output sample time points, in seconds, where
%                 Q.tVec(1) = R.tVec(1), Q.tVec(M) = R.tVec(N), and M =
%                 (N-1)*oversampFact + 1.
%
%         state = State of the quad at times in tVec, expressed as a
%                 structure with the following elements:
%
%                rMat = Mx3 matrix composed such that rMat(k,:)' is the 3x1
%                       position at tVec(k) in the I frame, in meters.
%
%                eMat = Mx3 matrix composed such that eMat(k,:)' is the 3x1
%                       vector of Euler angles at tVec(k), in radians,
%                       indicating the attitude.
%
%                vMat = Mx3 matrix composed such that vMat(k,:)' is the 3x1
%                       velocity at tVec(k) with respect to the I frame
%                       and expressed in the I frame, in meters per
%                       second.
%
%           omegaBMat = Mx3 matrix composed such that omegaBMat(k,:)' is the
%                       3x1 angular rate vector expressed in the body frame in
%                       radians, that applies at tVec(k).
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:
%+==============================================================================+

% Initialize.
N = length(R.tVec);
M = (N-1)*floor(S.oversampFact)+1;

Q.tVec            = nan(M,1);
Q.state.rMat      = nan(M,3);
Q.state.eMat      = nan(M,3);
Q.state.vMat      = nan(M,3);
Q.state.omegaBMat = nan(M,3);

% Run ode45.
stateinit_k( 1: 3) = S.state0.r;
stateinit_k( 4: 6) = S.state0.v;
stateinit_k( 7:15) = reshape(euler2dcm(S.state0.e), 9,1);
stateinit_k(16:18) = S.state0.omegaB;
stateinit_k(19:22) = zeros(4,1);

for k = 1:N-1

    % Grab current reference trajectory. This is a ZOH, no time delay or
    % advance.
    R_k.rIstark = R.rIstar(k,:)';
    R_k.vIstark = R.vIstar(k,:)';
    R_k.aIstark = R.aIstar(k,:)';
    R_k.xIstark = R.xIstar(k,:)';

    % Grab current state.
    S_k.statek.rI     =         stateinit_k( 1: 3)';
    S_k.statek.vI     =         stateinit_k( 4: 6)';
    S_k.statek.RBI    = reshape(stateinit_k( 7:15), 3,3);
    S_k.statek.omegaB =         stateinit_k(16:18)';

    % Calculate controllers.
    [cmd_force, cmd_bodyz] = trajectoryController(R_k, S_k, P); R_k.zIstark = cmd_bodyz;
     cmd_torque            = attitudeController  (R_k, S_k, P);

    % ODE inputs.
    dt_in  = R.tVec(k+1)-R.tVec(k);
    dt_out = dt_in/S.oversampFact;

    eaVec_k   = voltageConverter(cmd_force, cmd_torque, P);
    distVec_k = S.distMat(k,:)';
    tspan_k   = R.tVec(k):dt_out:R.tVec(k+1);

    [tvec_k, state_k] = ode45(                @(t,X)         ...
                              quadOdeFunctionHF(t,X,         ...
                                                eaVec_k,     ...
                                                distVec_k,   ...
                                                P),          ...
                              tspan_k,                       ...
                              stateinit_k);
    % ODE outputs.
    stateinit_k = state_k(end,:);

    % Fill in output data.
    L = length(tvec_k);
    indices = (k-1)*(L-1)+1:k*(L-1)+1;

    Q.tVec           (indices  ) = tvec_k;
    Q.state.rMat     (indices,:) = state_k(:, 1: 3);
    Q.state.vMat     (indices,:) = state_k(:, 4: 6);
    Q.state.omegaBMat(indices,:) = state_k(:,16:18);

    % Get and fill Euler angles.
    euler_k = nan(L,3);
    for j = 1:L
        RBI = reshape(state_k(j,7:15), 3,3);
        euler_k(j,:) = dcm2euler(RBI);
    end
    Q.state.eMat(indices,:) = euler_k;

end

end
function [Xdot] = quadOdeFunction(t,X,omegaVec,distVec,P) %#ok<INUSL> 
% quadOdeFunction : Ordinary differential equation function that models
%                   quadrotor dynamics.  For use with one of Matlab's ODE
%                   solvers (e.g., ode45).
%
%
% INPUTS
%
% t ---------- Scalar time input, as required by Matlab's ODE function
%              format.
%
% X ---------- Nx-by-1 quad state, arranged as 
%
%              X = [rI',vI',RBI(1,1),RBI(2,1),...,RBI(2,3),RBI(3,3),omegaB']'
%
%              rI = 3x1 position vector in I in meters
%              vI = 3x1 velocity vector wrt I and in I, in meters/sec
%             RBI = 3x3 attitude matrix from I to B frame
%          omegaB = 3x1 angular rate vector of body wrt I, expressed in B
%                   in rad/sec
%
% omegaVec --- 4x1 vector of rotor angular rates, in rad/sec.  omegaVec(i)
%              is the constant rotor speed setpoint for the ith rotor.
%
%  distVec --- 3x1 vector of constant disturbance forces acting on the quad's
%              center of mass, expressed in Newtons in I.
%
% P ---------- Structure with the following elements:
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m 
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m 
%
% OUTPUTS
%
% Xdot ------- Nx-by-1 time derivative of the input vector X
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+

g = P.constants.g;

% Parse input.
vI     = X( 4: 6);
RBIvec = X( 7:15);
omegaB = X(16:18);

RBI = reshape(RBIvec, 3,3);
[U,~,V] = svd(RBI);
RBI = U*V';

% Dynamics.
s  = -P.quadParams.omegaRdir;
ri =  P.quadParams.rotor_loc;
m  =  P.quadParams.m;
J  =  P.quadParams.Jq;
Fi =  P.quadParams.kF.*omegaVec.^2;
Ni =  P.quadParams.kN.*omegaVec.^2;

Fi = [zeros(2,4);    Fi'];
Ni = [zeros(2,4); s.*Ni'];

FB = sum(Fi,2);
NB = sum(Ni+cross(ri,Fi),2);

Xdot = nan(18,1);
Xdot( 1: 3) = vI;
Xdot( 4: 6) = -[0 0 g]' + 1/m*RBI'*FB + 1/m*distVec;
Xdot(16:18) = eye(3)/J*(NB-crossProductEquivalent(omegaB)*J*omegaB);

% Attitude.
RBIdot = -crossProductEquivalent(omegaB)*RBI;
Xdot(7:15) = reshape(RBIdot, 9,1);

end
function [Xdot] = quadOdeFunctionHF(t,X,eaVec,distVec,P) %#ok<INUSL> 
% quadOdeFunctionHF : Ordinary differential equation function that models
%                     quadrotor dynamics -- high-fidelity version.  For use
%                     with one of Matlab's ODE solvers (e.g., ode45).
%
%
% INPUTS
%
% t ---------- Scalar time input, as required by Matlab's ODE function
%              format.
%
% X ---------- Nx-by-1 quad state, arranged as 
%
%              X = [rI',vI',RBI(1,1),RBI(2,1),...,RBI(2,3),RBI(3,3),...
%                   omegaB',omegaVec']'
%
%              rI = 3x1 position vector in I in meters
%              vI = 3x1 velocity vector wrt I and in I, in meters/sec
%             RBI = 3x3 attitude matrix from I to B frame
%          omegaB = 3x1 angular rate vector of body wrt I, expressed in B
%                   in rad/sec
%        omegaVec = 4x1 vector of rotor angular rates, in rad/sec.
%                   omegaVec(i) is the angular rate of the ith rotor.
%
%    eaVec --- 4x1 vector of voltages applied to motors, in volts.  eaVec(i)
%              is the constant voltage setpoint for the ith rotor.
%
%  distVec --- 3x1 vector of constant disturbance forces acting on the quad's
%              center of mass, expressed in Newtons in I.
%
% P ---------- Structure with the following elements:
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m 
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m 
%
% OUTPUTS
%
% Xdot ------- Nx-by-1 time derivative of the input vector X
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+

% Parse input.
vI     = X( 4: 6);                      % velocity, inertial
RBIvec = X( 7:15);                      % rotation matrix I->B, vectorized
omegaB = X(16:18);                      % body angular rates
omegaP = X(19:22);                      % propeller rotation rates

ri =  P.quadParams.rotor_loc;           % m, rotor locations in body frame
s  = -P.quadParams.omegaRdir;           % unitless, directionality of rotors
m  =  P.quadParams.m;                   % kg, quadrotor total mass
J  =  P.quadParams.Jq;                  % kg-m^2, quadrotor inertia tensor

% Attitude. Normalize RBI, then find rate of change.
RBI = reshape(RBIvec, 3,3);
RIB = RBI';
[U,~,V] = svd(RBI);
RBI = U*V';
RBIdot = -crossProductEquivalent(omegaB)*RBI;

% Motor dynamics.
alphaVec = 1./P.quadParams.taum .* ...
    (-omegaP + P.quadParams.cm.*eaVec); % rad/s^2, propeller rotation angular acceleration

% Flight effectors, scalar.
Fp = P.quadParams.kF.*omegaP.^2;        % N, thrust forces from props
Np = P.quadParams.kN.*omegaP.^2;        % N-m, chopping torques from props
zI = RIB * [0 0 1]';                    % unitless, unit vector of body Z-axis in inertial frame
Fd = 1/2 * P.quadParams.Cd ...
         * P.quadParams.Ad ...
         * P.constants.rho ...
         * dot(zI,vI)            ;      % N, aerodynamic drag force.
                                        % NOTE: Not full magnitude, vI left out for implementation purposes.
Fg = P.quadParams.m*P.constants.g;      % N, weight

% Convert scalar forces to vectors.
Fp = [zeros(2,4);    Fp'];
Np = [zeros(2,4); s.*Np'];
Fd = Fd * -vI;
Fg = Fg * [0 0 -1]';

FB = sum(Fp,2);                         % all body frame forces
FI = sum(Fd + Fg + distVec,2);          % all inertial frame forces
NB = sum(Np+cross(ri,Fp),2);            % all body frame torques

% Populate output.
Xdot        = nan(size(X));
Xdot( 1: 3) = X(4:6);                   % drI/dt
Xdot( 4: 6) = 1/m*(RIB*FB + FI);        % dvI/dt
Xdot( 7:15) = reshape(RBIdot, 9,1);     % dRBI/dt
Xdot(16:18) = eye(3)/J*(NB-crossProductEquivalent(omegaB)*J*omegaB);
                                        % dωbody/dt
Xdot(19:22) = alphaVec;                 % dωprop/dt

end
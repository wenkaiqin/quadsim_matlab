function [P] = simulateQuadrotorDynamicsHF(S)
% simulateQuadrotorDynamicsHF : Simulates the dynamics of a quadrotor
%                               aircraft (high-fidelity version).
%
%
% INPUTS
%
% S ---------- Structure with the following elements:
%
%          tVec = Nx1 vector of uniformly-sampled time offsets from the
%                 initial time, in seconds, with tVec(1) = 0.
%
%  oversampFact = Oversampling factor. Let dtIn = tVec(2) - tVec(1). Then the
%                 output sample interval will be dtOut =
%                 dtIn/oversampFact. Must satisfy oversampFact >= 1.   
%
%         eaMat = (N-1)x4 matrix of motor voltage inputs.  eaMat(k,j) is the
%                 constant (zero-order-hold) voltage for the jth motor over
%                 the interval from tVec(k) to tVec(k+1).
%
%        state0 = State of the quad at tVec(1) = 0, expressed as a structure
%                 with the following elements:
%                   
%                   r = 3x1 position in the world frame, in meters
% 
%                   e = 3x1 vector of Euler angles, in radians, indicating the
%                       attitude
%
%                   v = 3x1 velocity with respect to the world frame and
%                       expressed in the world frame, in meters per second.
%                 
%              omegaB = 3x1 angular rate vector expressed in the body frame,
%                       in radians per second.
%
%       distMat = (N-1)x3 matrix of disturbance forces acting on the quad's
%                 center of mass, expressed in Newtons in the world frame.
%                 distMat(k,:)' is the constant (zero-order-hold) 3x1
%                 disturbance vector acting on the quad from tVec(k) to
%                 tVec(k+1).
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m 
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m 
%
%
% OUTPUTS
%
% P ---------- Structure with the following elements:
%
%          tVec = Mx1 vector of output sample time points, in seconds, where
%                 P.tVec(1) = S.tVec(1), P.tVec(M) = S.tVec(N), and M =
%                 (N-1)*oversampFact + 1.
%                  
%  
%         state = State of the quad at times in tVec, expressed as a structure
%                 with the following elements:
%                   
%                rMat = Mx3 matrix composed such that rMat(k,:)' is the 3x1
%                       position at tVec(k) in the world frame, in meters.
% 
%                eMat = Mx3 matrix composed such that eMat(k,:)' is the 3x1
%                       vector of Euler angles at tVec(k), in radians,
%                       indicating the attitude.
%
%                vMat = Mx3 matrix composed such that vMat(k,:)' is the 3x1
%                       velocity at tVec(k) with respect to the world frame
%                       and expressed in the world frame, in meters per
%                       second.
%                 
%           omegaBMat = Mx3 matrix composed such that omegaBMat(k,:)' is the
%                       3x1 angular rate vector expressed in the body frame in
%                       radians, that applies at tVec(k).
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+  

% Initialize.
N = length(S.tVec);
M = (N-1)*(floor(S.oversampFact))+1;

P.tVec            = nan(M,1);
P.state.rMat      = nan(M,3);
P.state.eMat      = nan(M,3);
P.state.vMat      = nan(M,3);
P.state.omegaBMat = nan(M,3);

stateinit_k( 1: 3) = S.state0.r;
stateinit_k( 4: 6) = S.state0.v;
stateinit_k( 7:15) = reshape(euler2dcm(S.state0.e), 9,1);
stateinit_k(16:18) = S.state0.omegaB;
stateinit_k(19:22) = zeros(4,1);

% Run ode45.
for k = 1:N-1
    
    dt_in  = S.tVec(k+1)-S.tVec(k);
    dt_out = dt_in/S.oversampFact;

    % ODE inputs.
    eaVec_k    = S.eaMat  (k,:)';
    distVec_k  = S.distMat(k,:)';
    tspan_k    = S.tVec(k):dt_out:S.tVec(k+1);

    [tvec_k, state_k] = ode45(                @(t,X)         ...
                              quadOdeFunctionHF(t,X,         ...
                                                eaVec_k,     ...
                                                distVec_k,   ...
                                                S),          ... 
                              tspan_k,                       ...
                              stateinit_k);

    % ODE outputs.
    stateinit_k = state_k(end,:);

    % Fill in output data.
    L = length(tvec_k);
    indices = (k-1)*(L-1)+1:k*(L-1)+1;

    P.tVec           (indices  ) = tvec_k;
    P.state.rMat     (indices,:) = state_k(:, 1: 3);
    P.state.vMat     (indices,:) = state_k(:, 4: 6);
    P.state.omegaBMat(indices,:) = state_k(:,16:18);

    % Get and fill Euler angles.
    euler_k = nan(L,3);
    for j = 1:L
        RBI = reshape(state_k(j,7:15), 3,3);
        euler_k(j,:) = dcm2euler(RBI);
    end
    P.state.eMat(indices,:) = euler_k;
    
end


end
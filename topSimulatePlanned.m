% Top-level script for calling simulateQuadrotorControl or
% simulateQuadrotorEstimationAndControl

%#ok<*UNRCH>

% 'clear all' is needed to clear out persistent variables from run to run
clear all; clc; %#ok<CLALL> 

% Seed Matlab's random number: this allows you to simulate with the same noise
% every time (by setting a nonnegative integer seed as argument to rng) or
% simulate with a different noise realization every time (by setting
% 'shuffle' as argument to rng).
rng('shuffle');
% rng(1234);

% Load planned data.
load('position.mat')
load('velocity.mat')
load('acceleration.mat')

% Assert this flag to call the full estimation and control simulator;
% otherwise, only the control simulator is called
estimationFlag = 1;

Tsim = max(pos_t);          % Total simulation time, in seconds
delt = 1/200;               % Update interval, in seconds
N    = floor(Tsim/delt)+1;
tVec = (0:N-1)'*delt;       % Time vector, in seconds 
n = 2*pi/10;                % Angular rate of orbit, in rad/sec
r = 2;                      % Radius of circle, in meters

if min(abs(pos_t-tVec)) > 1e-13
    error('Planned time vector is not compatible.')
end

% Populate reference trajectory
R.tVec = pos_t;
R.rIstar = [pos_x, pos_y, zeros(N,1)];
R.vIstar = [vel_x, vel_y, zeros(N,1)];
R.aIstar = [acc_x, acc_y, zeros(N,1)];

% The desired xI points toward the origin. The code below also normalizes
% each row in R.xIstar.
% R.xIstar = diag(1./vecnorm(R.rIstar'))*(-R.rIstar);
R.xIstar = [ones(N,1), zeros(N,1), zeros(N,1)];

% Matrix of disturbance forces acting on the body, in Newtons, expressed in I
S.distMat = 0*randn(N-1,3);
S.state0.r      = [0 0 0]';   % Initial position in m
S.state0.e      = [0 0 0]';   % Initial attitude expressed as Euler angles, in radians
S.state0.v      = [0 0 0]';   % Initial velocity of body with respect to I, expressed in I, in m/s
S.state0.omegaB = [0 0 0]';   % Initial angular rate of body with respect to I, expressed in B, in rad/s
S.oversampFact  =       10;   % Oversampling factor
S.rXIMat = [0,0,0; 0,0,0.7];  % Feature locations in the I frame

% Quadrotor parameters and constants
quadParamsScript;
constantsScript;
sensorParamsScript;
P.quadParams   = quadParams; 
P.constants    = constants; 
P.sensorParams = sensorParams;

if(estimationFlag)
  Q = simulateQuadrotorEstimationAndControl(R,S,P);
else
  Q = simulateQuadrotorControl(R,S,P);
end

S2.tVec = Q.tVec;
S2.rMat = Q.state.rMat;
S2.eMat = Q.state.eMat;
S2.plotFrequency = 20;
S2.makeGifFlag = true;
S2.gifFileName = 'testGif.gif';
S2.bounds = -4.5*ones(1,6)+10*[-1 1 -1 1 -1 1];
visualizeQuad(S2);

%% Plot.
figure(2), clf
plot(Q.tVec,Q.state.rMat(:,3)); grid on;hold on
xlabel('Time (sec)');
ylabel('Vertical (m)');
title('Vertical position of CM'); 

figure(3), clf
psiError = unwrap(n*Q.tVec + pi - Q.state.eMat(:,3));
meanPsiErrorInt = round(mean(psiError)/2/pi);
plot(Q.tVec,psiError - meanPsiErrorInt*2*pi);
grid on; hold on
xlabel('Time (sec)');
ylabel('\Delta \psi (rad)');
title('Yaw angle error');

figure(5), clf
axis equal, grid on, hold on
plot(pos_x, pos_y, 'r--')
plot(Q.state.rMat(:,1), Q.state.rMat(:,2), 'k-'); 
xlabel('X (m)');
ylabel('Y (m)');
title('Horizontal position of CM');
legend('Planned','Achieved')

% plotQuadDynamics(Q)

% figure(1), sgtitle('')

% % Get the body X axis in inertial frame. Loop through all times with a
% % decimation factor. Formulate the decimation factor based on the frequency
% % of desired plot.
% dec_interval = .1;
% dec_factor   = floor(dec_interval*S.oversampFact/delt);
% 
% % Begin with largest possible matrix, filter out all nan later.
% bodyX_i = nan(size(Q.state.eMat));
% for i_time=1:dec_factor:length(Q.tVec)
%     RBI = euler2dcm(Q.state.eMat(i_time,:)');
%     bodyX_i(i_time,:) = RBI' * [1 0 0]';
% end
% % Take body [X,Y] position at the same times we got the Euler angles.
% bodypos = Q.state.rMat(~isnan(bodyX_i));
% bodypos = reshape(bodypos, length(bodypos)/3,3);
% % Filter out all nan.
% bodyX_i = rmmissing(bodyX_i);
% 
% figure(20), clf
% hold on, grid on
% plot(Q.state.rMat(:,1),Q.state.rMat(:,2),'k')
% axis equal
% quiver(bodypos(:,1), bodypos(:,2), ...
%        bodyX_i(:,1), bodyX_i(:,2), .5, 'r')
% % title('Trajectory Topview')
% legend('Trajectory','Body X Axis')
% xlabel('Inertial X (m)')
% ylabel('Inertial Y (m)')
% xlim(xlim*1.2)
% 
% figure(21), clf
% grid on, hold on
% plot(Q.tVec, Q.state.rMat(:,3))
% xlabel('Time (s)')
% ylabel('Inertial Z Position (m)')
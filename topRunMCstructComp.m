% Top-level script for calling simulateQuadrotorControl or
% simulateQuadrotorEstimationAndControl

% 'clear all' is needed to clear out persistent variables from run to run
clear all; clc %#ok<CLALL> 
rng('shuffle')
% rng(1234);

Tsim =    10;               % Total simulation time, in seconds
delt = 0.005;               % Update interval, in seconds
N    = floor(Tsim/delt);
tVec = (0:N-1)'*delt;       % Time vector, in seconds 

n = 2*pi/10;    % Angular rate of orbit, in rad/sec
r = 2;          % Radius of circle, in meters

% Populate reference trajectory
R.tVec = tVec;
R.rIstar = [r*cos(n*tVec),r*sin(n*tVec),ones(N,1)];
R.vIstar = [-r*n*sin(n*tVec),r*n*cos(n*tVec),zeros(N,1)];
R.aIstar = [-r*n*n*cos(n*tVec),-r*n*n*sin(n*tVec),zeros(N,1)];

% The desired xI points toward the origin. The code below also normalizes
% each row in R.xIstar.
R.xIstar = diag(1./vecnorm(R.rIstar'))*(-R.rIstar);

S.distMat       = 0*randn(N-1,3);   % Matrix of disturbance forces acting on the body, in Newtons, expressed in I
S.state0.r      = [r 0 0]';         % Initial position in m
S.state0.e      = [0 0 pi]';        % Initial attitude expressed as Euler angles, in radians
S.state0.v      = [0 0 0]';         % Initial velocity of body with respect to I, expressed in I, in m/s
S.state0.omegaB = [0 0 0]';         % Initial angular rate of body with respect to I, expressed in B, in rad/s
S.oversampFact  = 2;                % Oversampling factor
S.rXIMat        = [0,0,0; 0,0,0.7]; % Feature locations in the I frame

% Quadrotor parameters and constants
quadParamsScript;
constantsScript;
sensorParamsScript;
P.quadParams = quadParams; 
P.constants = constants;
P.sensorParams = sensorParams;

num_mc = 100;

% Error in structure computation, 2-norm of RXIest - RXItrue.
error_cam = nan(num_mc, 1);
for i_mc = 1:num_mc

    fprintf('%d/%d runs done...\n', i_mc, num_mc)

    % Run trial.
    [Q,Ms] = simulateQuadrotorEstimationAndControl(R,S,P);

    % Determine error.
    [RXI_est, ~] = estimate3dFeatureLocation(Ms,P);
    error_cam(i_mc) = norm(RXI_est-S.rXIMat(2,:)');
    
end

%% Plot.
histogram(error_cam)
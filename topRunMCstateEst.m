% Top-level script for calling simulateQuadrotorControl or
% simulateQuadrotorEstimationAndControl

% 'clear all' is needed to clear out persistent variables from run to run
clear all; clc; %#ok<CLALL> 
% rng('shuffle');
rng(1234);

Tsim =    10;               % Total simulation time, in seconds
delt = 0.005;               % Update interval, in seconds
N    = floor(Tsim/delt);
tVec = (0:N-1)'*delt;       % Time vector, in seconds 

n = 2*pi/10;    % Angular rate of orbit, in rad/sec
r = 2;          % Radius of circle, in meters

% Populate reference trajectory
R.tVec = tVec;
R.rIstar = [r*cos(n*tVec),r*sin(n*tVec),ones(N,1)];
R.vIstar = [-r*n*sin(n*tVec),r*n*cos(n*tVec),zeros(N,1)];
R.aIstar = [-r*n*n*cos(n*tVec),-r*n*n*sin(n*tVec),zeros(N,1)];

% The desired xI points toward the origin. The code below also normalizes
% each row in R.xIstar.
R.xIstar = diag(1./vecnorm(R.rIstar'))*(-R.rIstar);

S.distMat       = 0*randn(N-1,3);   % Matrix of disturbance forces acting on the body, in Newtons, expressed in I
S.state0.r      = [r 0 0]';         % Initial position in m
S.state0.e      = [0 0 pi]';        % Initial attitude expressed as Euler angles, in radians
S.state0.v      = [0 0 0]';         % Initial velocity of body with respect to I, expressed in I, in m/s
S.state0.omegaB = [0 0 0]';         % Initial angular rate of body with respect to I, expressed in B, in rad/s
S.oversampFact  = 10;                % Oversampling factor
S.rXIMat        = [0,0,0; 0,0,0.7]; % Feature locations in the I frame

% Quadrotor parameters and constants
quadParamsScript;
constantsScript;
sensorParamsScript;
P.quadParams = quadParams; 
P.constants = constants;
P.sensorParams = sensorParams;

num_mc = 5;

nom_plot = 'k';
mc_plot = 'r--';

figure(2), clf
Q = simulateQuadrotorControl(R,S,P);

plot(Q.tVec,Q.state.rMat(:,3), nom_plot)
hold on, grid on
xlabel('Time (sec)');
ylabel('Vertical (m)');
title('Vertical position of CM');

figure(3), clf
psiError = unwrap(n*Q.tVec + pi - Q.state.eMat(:,3));
meanPsiErrorInt = round(mean(psiError)/2/pi);
plot(Q.tVec,psiError - meanPsiErrorInt*2*pi, nom_plot);
hold on, grid on
xlabel('Time (sec)');
ylabel('\Delta \psi (rad)');
title('Yaw angle error');

figure(5), clf
plot(Q.state.rMat(:,1), Q.state.rMat(:,2), nom_plot);
axis equal, hold on, grid on
xlabel('X (m)');
ylabel('Y (m)');
title('Horizontal position of CM');

for i_mc = 1:num_mc

    Q = simulateQuadrotorEstimationAndControl(R,S,P);

    figure(2)
    plot(Q.tVec,Q.state.rMat(:,3), mc_plot);

    figure(3)
    psiError = unwrap(n*Q.tVec + pi - Q.state.eMat(:,3));
    meanPsiErrorInt = round(mean(psiError)/2/pi);
    plot(Q.tVec,psiError - meanPsiErrorInt*2*pi, mc_plot);

    figure(5)
    plot(Q.state.rMat(:,1), Q.state.rMat(:,2), mc_plot);
    

end

for i_fig = [2 3 5]
    figure(i_fig)
    legend('Nominal','MC')
end
function [NBk] = attitudeController(R,S,P)
% attitudeController : Controls quadcopter toward a reference attitude
%
%
% INPUTS
%
% R ---------- Structure with the following elements:
%
%       zIstark = 3x1 desired body z-axis direction at time tk, expressed as a
%                 unit vector in the I frame.
%
%       xIstark = 3x1 desired body x-axis direction, expressed as a
%                 unit vector in the I frame.
%
% S ---------- Structure with the following elements:
%
%        statek = State of the quad at tk, expressed as a structure with the
%                 following elements:
%                   
%                  rI = 3x1 position in the I frame, in meters
% 
%                 RBI = 3x3 direction cosine matrix indicating the
%                       attitude
%
%                  vI = 3x1 velocity with respect to the I frame and
%                       expressed in the I frame, in meters per second.
%                 
%              omegaB = 3x1 angular rate vector expressed in the body frame,
%                       in radians per second.
%
% P ---------- Structure with the following elements:
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m 
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m 
%
%
% OUTPUTS
%
% NBk -------- Commanded 3x1 torque expressed in the body frame at time tk, in
%              N-m.
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+  

% Define gain matrices.
% TH values.
% K = 1.3*diag([0.8 0.8 0.2]);
% Kd = 0.7*diag([0.4 0.4 0.1]);

% WQ values.
K  = diag([.32 .80 1.1]);
Kd = diag([.08 .16 .24]);

% Finish formulation of commanded body basis in inertial frame, then stack
% them together for inertial -> body rotation. Then, form rotation error
% as:
%
%     Rerror = RBIcommand * RIBcurrent
%
% Rerror is formulated to satisfy:
%
%     RBIcommand = Rerror * RBIcurrent
%
% Finally, form error vector      [e23 - e32
%                            eE =  e31 - e13
%                                  e12 - e21], where eij represents the
%                                              elements of Rerror.
%
yIstark = cross(R.zIstark,R.xIstark)/norm(cross(R.zIstark, R.xIstark));
RBIstar = [cross(yIstark,R.zIstark) yIstark R.zIstark]';
Rerror = RBIstar * S.statek.RBI';

eE = [Rerror(2,3) - Rerror(3,2)
      Rerror(3,1) - Rerror(1,3)
      Rerror(1,2) - Rerror(2,1)];

% Form required torque.
wB = S.statek.omegaB;
NBk = K*eE - Kd*wB + crossProductEquivalent(wB)*P.quadParams.Jq*wB;

end
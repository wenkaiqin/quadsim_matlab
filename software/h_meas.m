% function [zk] = h_meas(xk,wk,RBIBark,rXIMat,mcVeck,P)
% h_meas : Measurement model for quadcopter.
%
% INPUTS
%
% xk --------- 15x1 state vector at time tk, defined as 
% 
%              xk = [rI', vI', e', ba', bg']'
%
%              where all corresponding quantities are identical to those
%              defined for E.statek in stateEstimatorUKF.m and where e is the
%              3x1 error Euler angle vector defined such that for an estimate
%              RBIBar of the attitude, the true attitude is RBI =
%              dRBI(e)*RBIBar, where dRBI(e) is the DCM formed from the error
%              Euler angle vector e.
%
% wk --------- nz-by-1 measurement noise vector at time tk, defined as
%
%              wk = [wpIk', wbIk', w1C', w2C', ..., wNfkC']'
%
%              where nz = 6 + Nfk*3, with Nfk being the number of features
%              measured by the camera at time tk, and where all 3x1 noise
%              vectors represent additive noise on the corresponding
%              measurements.
%
% RBIBark ---- 3x3 attitude matrix estimate at time tk.
%
% rXIMat ----- Nf-by-3 matrix of coordinates of visual features in the
%              simulation environment, expressed in meters in the I
%              frame. rXIMat(i,:)' is the 3x1 vector of coordinates of the ith
%              feature.
%
% mcVeck ----- Nf-by-1 vector indicating whether the corresponding feature in
%              rXIMat is sensed by the camera: If mcVeck(i) is true (nonzero),
%              then a measurement of the visual feature with coordinates
%              rXIMat(i,:)' is assumed to be made by the camera.  mcVeck
%              should have Nfk nonzero values.
%
% P ---------- Structure with the following elements:
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m 
%
%  sensorParams = Structure containing sensor parameters, as defined in
%                 sensorParamsScript.m
%
%
% OUTPUTS
%
% zk --------- nz-by-1 measurement vector at time tk, defined as
%
%              zk = [rpItilde', rbItildeu', v1Ctildeu', ..., vNfkCtildeu']'
%
%              where rpItilde is the 3x1 measured position of the primary
%              antenna in the I frame, rbItildeu is the 3x1 measured unit
%              vector pointing from the primary to the secondary antenna,
%              expressed in the I frame, and viCtildeu is the 3x1 unit vector,
%              expressed in the camera frame, pointing toward the ith 3D
%              feature, which has coordinates rXIMat(i,:)'.
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author: 
%+==============================================================================+

% % Parse inputs.
% rIk = xk(1:3);
% % vIk = xk(4:6);
% ek  = xk(7:9);
% 
% % bak = xk(10:12);
% % bgk = xk(13:15);
% 
% % nz  = size(wk,1);   % Length of measurement noise vector.
% % Nfk = nz/3-2;       % Number of camera observations.
% Nf = size(mcVeck,1);
% 
% wpIk = wk(1:3);     % Antenna 1 position noise.
% wbIk = wk(4:6);     % Baseline unit vector noise.
% wCk  = wk(7:end);   % Feature pointing vector noise.
% 
% % Rotation matrices.
% RCB = P.sensorParams.RCB;
% RBI = euler2dcm(ek) * RBIBark;
% RIB = RBI';
% 
% % Grab antenna positions.
% ra1B = P.sensorParams.raB(:,1);     % Primary, body frame.
% ra2B = P.sensorParams.raB(:,2);     % Secondary.
% rbB  = ra2B - ra1B;                 % Baseline vector from primary -> secondary.
% rbBu = rbB/norm(rbB);               % Unit baseline.
% 
% % Process camera measurements.
% 
% % Find the unit pointing vector from the camera center to each feature
% % point seen by the camera, then convert to the camera frame.
% rcIk = rIk + RIB*P.sensorParams.rocB;       % Camera position in inertial.
% vIk  = rXIMat - rcIk';                      % Pointing vector from camera center -> all feature points.
% vIku = vIk./sqrt(sum(vIk.^2,2));            % Unit pointing.
% 
% % Filter out the unobserved features. Use a wide filter matrix that is Nfk x Nf,
% % noting that Nfk < Nf. Begin with the diagonal form of detection matrix
% % mcVeck, then eliminate all rows that have only zeros.
% filter = diag(mcVeck);                          % Nf x Nf
% detect = ~all(filter(:,1:Nf)==zeros(1,Nf), 2);  % Nf x 1
% filter = filter(detect,:);                      % Nfk x Nf
% vIku   = filter*vIku;                           % Nf x 3 => Nfk x 3
% vCku   = RCB*RBI*vIku';                         % 3 x Nfk.
% 
% % Form measurement model.
% rpItilde  = rIk + RIB*ra1B + wpIk;
% rbItildeu =       RIB*rbBu + wbIk;
% vCtildeu  = vCku(:)        + wCk ;
% 
% zk = [rpItilde; rbItildeu; vCtildeu];
%
% end

%% Olivia
function [zk] = h_meas(xk,wk,RBIBark,rXIMat,mcVeck,P)
rI = xk(1:3, :); 
vI = xk(4:6, :); 
ek = xk(7:9, :); 
ba = xk(10:12, :); 
bg = xk(13:15, :); 

raB_primary = P.sensorParams.raB(:,1);
raB_secondary = P.sensorParams.raB(:,2);
rbB = raB_secondary - raB_primary; 
rbB_u = norm (rbB) / rbB; 

RCB = P.sensorParams.RCB; 
RBIk =  euler2dcm(ek)*RBIBark; 
rocB = P.sensorParams.rocB;
rcI = rI + RBIk.'*rocB;
vciC_u_total = []; 

for i = 1: size(rXIMat,1) 
    if abs(mcVeck(i)) > 0  
        vI1 = rXIMat(i,:)' - rcI; 
        vI1_u = vI1 / norm(vI1); 
        vciC_u =  RCB * RBIk * vI1_u; % might need to reverse RBIk*RCB*vI1_u
        vciC_u_total = [vciC_u_total; vciC_u]; 
    end 
end

rpl_tilde = rI + RBIk.'*raB_primary + wk(1:3); 
rbI_tilde_u = RBIk.'*rbB_u.'+wk(4:6); 
vIC_tilde = vciC_u_total+wk(7:end); 

hk = [rpl_tilde; rbI_tilde_u; vIC_tilde]; 
zk = hk +wk; % turn on!!!!!!
end 

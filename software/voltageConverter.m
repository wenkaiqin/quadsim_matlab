function [eak] = voltageConverter(Fk,NBk,P)
% voltageConverter : Generates output voltages appropriate for desired
%                    torque and thrust.
%
%
% INPUTS
%
% Fk --------- Commanded total thrust at time tk, in Newtons.
%
% NBk -------- Commanded 3x1 torque expressed in the body frame at time tk, in
%              N-m.
%
% P ---------- Structure with the following elements:
%
%    quadParams = Structure containing all relevant parameters for the
%                 quad, as defined in quadParamsScript.m 
%
%     constants = Structure containing constants used in simulation and
%                 control, as defined in constantsScript.m 
%
%
% OUTPUTS
%
% eak -------- Commanded 4x1 voltage vector to be applied at time tk, in
%              volts. eak(i) is the voltage for the ith motor.
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+  

alpha_cur = 1.0;
alpha_inc = 0.01;
alpha_min = 0.10;
beta  = 0.9;
kT = P.quadParams.kN./P.quadParams.kF;

% Max force of each propeller calculated from max voltage.
max_rate  = P.quadParams.cm*P.quadParams.eamax;
max_force = min(P.quadParams.kF.*max_rate.^2);

% Generate required forces at each rotor to achieve commanded force Fk and
% commanded torque NBk. Approach by inverting the relationship 
%     G*Frotor = [Fk NBk]
G = [ ones(1,4)
      P.quadParams.rotor_loc(2,:)
     -P.quadParams.rotor_loc(1,:)
     -kT'.*P.quadParams.omegaRdir];

rotor_force = inf(4,1);
while max(rotor_force)>max_force

    if alpha_cur<alpha_min
        warning('Unable to find required rotor forces.')
        rotor_force = zeros(4,1);
        continue
    end

%     if alpha_cur < 1
%         fprintf('Warning: motors saturated. α = %.2f\n', alpha_cur)
%     end

    % Saturate commandded force at 4Fmax, with beta coefficient allocater.
    command = [min(Fk, 4*beta*max_force); alpha_cur*NBk];
    rotor_force = G\command;
 
    % If any required rotor forces are greater than the saturation limit, then
    % regenerate with less commanded torque.
    alpha_cur = alpha_cur - alpha_inc;

end

% After settling on a set of rotor forces, convert to voltage command.
rotor_force(rotor_force<0) = 0;
rotor_rate = sqrt(rotor_force./P.quadParams.kF);
eak = rotor_rate./P.quadParams.cm;

end
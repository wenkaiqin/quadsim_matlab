function [rXIHat,Px] = estimate3dFeatureLocation(M,P)
% estimate3dFeatureLocation : Estimate the 3D coordinates of a feature point
%                             seen by two or more cameras with known pose.
%
%
% INPUTS
%
% M ---------- Structure with the following elements:
%
%       rxArray = 1xN cell array of measured positions of the feature point
%                 projection on the camera's image plane, in pixels.
%                 rxArray{i} is the 2x1 vector of coordinates of the feature
%                 point as measured by the ith camera.  To ensure the
%                 estimation problem is observable, N must satisfy N >= 2 and
%                 at least two cameras must be non-colinear.
%
%      RCIArray = 1xN cell array of I-to-camera-frame attitude matrices.
%                 RCIArray{i} is the 3x3 attitude matrix corresponding to the
%                 measurement rxArray{i}.
%
%       rcArray = 1xN cell array of camera center positions.  rcArray{i} is
%                 the 3x1 position of the camera center corresponding to the
%                 measurement rxArray{i}, expressed in the I frame in meters.
%
% P ---------- Structure with the following elements:
%
%  sensorParams = Structure containing all relevant parameters for the quad's
%                 sensors, as defined in sensorParamsScript.m
%
% OUTPUTS
%
%
% rXIHat -------- 3x1 estimated location of the feature point expressed in I
%                 in meters.
%
% Px ------------ 3x3 error covariance matrix for the estimate rxIHat.
%
%+------------------------------------------------------------------------------+
% References:
%
%
% Author:  
%+==============================================================================+ 

% Load camera projection matrix.

% Solve the structure computation method as follows:
% 
% We know that expressed with homogeneous coordinates, we have
%
%    x [cross] PX = 0
%
% Where x is the feature point's homogeneous planar projection into the
% camera plane [3x1], P is the camera projection matrix [3x4], and X is the
% true homogeneous 3D location of the feature point in I frame [4x1].
%
% With some noise and linear algebra,
%
%    [xp~*p1_3T - p1_1T]       [w1_1]   [0]
%    |yp~*p1_3T - p1_2T| * X + |w1_2| = |0|
%    |xs~*p2_3T - p2_1T|       |w2_1|   |0|
%    [ys~*p2_3T - p2_2T]       [w2_2]   [0]
%    {        H        }       { w }
%       
% where x~ and y~ represent the useful elements of the homogeneous x, p1_i
% and p2_i represent the projection matrices' rows corresponding to two
% different poses, and w1_i and w2_i representing the noise in each axis on the
% two different poses. This formulation can be vertically extended at will.
%
% Reformat as:
%
%            H   *   X    + w = 0
%    [Hr[3x3] -z]*[Xr 1]' + w = 0
%                           z = Hr Xr + w
%
% This linear formulation is simple to optimally estimate in the
% least-squares sense.
%
%    Xr_est = (Hr' Rinv Hr)^-1 * (Hr' Rinv z)
%
% Where R is the error covariance matrix of the noise w. R can be
% approximated as:
%
%                 [Rc  0 ...  0]
%      R = pc^2 * | 0 Rc      0|
%                 | .    ...  0|
%                 [ 0  0 ... Rc]
%
% where pc is the pixel size in meters and Rc is the 2x2 error covariance
% matrix for the Gaussian image noise, in pixels^2.

n_meas = length(M.rxArray);
H = nan(2*n_meas, 4);
R = [];

% Iterate through each measurement.
for i_meas = 1:n_meas

    % Find the projection matrices for each measurement.
    RCI =  M.RCIArray{i_meas};
    t   = -M.RCIArray{i_meas}*M.rcArray{i_meas};
    K   = P.sensorParams.K;
    proj = K*[RCI t];

    % Build measurement and covariance matrices H and R for estimation problem.
    p1T = proj(1,:);
    p2T = proj(2,:);
    p3T = proj(3,:);

    ps = P.sensorParams.pixelSize;  % m/px
    rx = ps*M.rxArray{i_meas};      % m
    x_meas = rx(1);
    y_meas = rx(2);
    
    Hrows = 2*i_meas-1:2*i_meas;
    H(Hrows,:) = [x_meas*p3T - p1T
                  y_meas*p3T - p2T];

    Rc = P.sensorParams.Rc;
    R = blkdiag(R, ps^2*Rc);

end

Hr =  H(:,1:3);
z  = -H(:,  4);

% Final estimate.
rXIHat = (Hr'*(R\Hr))\(Hr'*(R\z));

% We can further form the error covariance matrix in our estimate as:
%
%    Px = (HrT Rinv Hr)^-1
% 
Px = inv(Hr'*(R\Hr));

end


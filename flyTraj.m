% FLY A CIRCLE!!!!!
% CLOSED LOOP1!!!!!
clear, clc
if strcmp(getenv('USER'), 'wenkaiqin')
    path(pathdef)
    addpath(genpath(pwd))
end

% Quadrotor parameters and constants.
quadParamsScript;
constantsScript;
% sensorParamsScript;
P.quadParams   = quadParams;
P.constants    = constants;
% P.sensorParams = sensorParams;

% Circle parameters.
circ_radius =  4; % m
circ_time   = 10; % s
circ_revs   =  1; % revolutions

% Calculated parameters.
circ_velocity = 2*pi*circ_radius/circ_time;
circ_acentr   = circ_velocity^2/circ_radius;
circ_angrate  = 2*pi/circ_time;

% Simulation parameters.
Tsim = circ_time*circ_revs;
delt = 0.001;
N    = floor(Tsim/delt)+1;

S.oversampFact = 10;
S.distMat      = zeros(N-1,3);

% Form circular trajectory. Take successive derivatives of the reference
% trajectory position to get reference velocity and acceleration.
R.tVec         = (0:N-1)'*delt;

R.rIstar       = [circ_radius  *[ cos(2*pi/circ_time*R.tVec),  ...
                                  sin(2*pi/circ_time*R.tVec)], ... 
                                  zeros(size(R.tVec))];
R.vIstar       = [circ_velocity*[-sin(2*pi/circ_time*R.tVec),  ...
                                  cos(2*pi/circ_time*R.tVec)], ... 
                                  zeros(size(R.tVec))];
R.aIstar       = [circ_acentr  *[-cos(2*pi/circ_time*R.tVec),  ...
                                 -sin(2*pi/circ_time*R.tVec)], ... 
                                  zeros(size(R.tVec))];
R.xIstar       = [1            *[-cos(2*pi/circ_time*R.tVec),  ...
                                 -sin(2*pi/circ_time*R.tVec)], ... 
                                  zeros(size(R.tVec))];

% Initial conditions.
S.state0.r      = [ 2 0 0]';
S.state0.e      = [ 0 0 pi/2]';
S.state0.v      = [ 0 0 0]';
S.state0.omegaB = zeros(3,1);

% Test response.
mode = 2;

switch mode

    case 0

        R.rIstar = zeros(size(R.rIstar)); %#ok<*UNRCH>
        R.vIstar = zeros(size(R.vIstar));
        R.aIstar = zeros(size(R.aIstar));
        R.xIstar = zeros(size(R.xIstar));
        R.xIstar(:,1) = 1;

        step_time = 2;
        idxs = find(R.tVec>step_time);
        R.xIstar(idxs,:) = repmat([0 1 0],length(idxs),1);

        R.rIstar(R.tVec>1,3) = 1;
        R.rIstar(R.tVec>4,2) = (R.tVec(R.tVec>4)-4)*.5;
        R.vIstar(R.tVec>4,2) = .5;

        S.state0.r = zeros(3,1);
        S.state0.e = zeros(3,1);
        S.state0.v = zeros(3,1);

    case 1

        % Ramp velocity up for 2 seconds, hold.
        R.rIstar = zeros(length(R.tVec),3); %#ok<*UNRCH>
        R.vIstar = zeros(length(R.tVec),3);
        R.aIstar = zeros(length(R.tVec),3);
        R.xIstar = zeros(length(R.tVec),3);
        R.xIstar(:,1) = 1;

        R.vIstar(:,1) = R.tVec*0.5;
        R.vIstar(R.vIstar(:,1)>1,1) = 1;
        idxs = R.tVec>max(R.tVec-2);
        R.vIstar(idxs,1) = 1-0.5*(R.tVec(idxs)-min(R.tVec(idxs)));
        R.rIstar = cumtrapz(R.tVec,R.vIstar);
        R.aIstar = [diff(R.vIstar)./diff(R.tVec); [0 0 0]];

        S.state0.r = zeros(3,1);
        S.state0.e = zeros(3,1);
        S.state0.v = zeros(3,1);

end

flag_plotref = true;
if flag_plotref

    figure(10), clf
    sgtitle('3D Reference Trajectory')
    hold on, grid on
    plot3(R.rIstar(:,1),R.rIstar(:,2),R.rIstar(:,3))
    view(3)
    axis equal
    lim = 10*[-1 1]; axis(repmat(lim, 1,3))
    xlabel('X (m)'), ylabel('Y (m)'), zlabel('Z (m)')

    figure(11), clf
    sgtitle('Reference Trajectory Time History')
    subplot(311)
    hold on, grid on
    plot(R.tVec, R.rIstar)
    legend('X','Y','Z', 'Location','northwest')
    ylabel('Position (m)')

    subplot(312)
    hold on, grid on
    plot(R.tVec, R.vIstar)
    ylabel('Velocity (m/s)')

    subplot(313)
    hold on, grid on
    plot(R.tVec, R.aIstar)
    ylabel('Acceleration (m/s^2)')
    xlabel('Time (s)')

end

% Simulate.
Q = simulateQuadrotorControl(R,S,P);

% Visualize.
S2.tVec          = Q.tVec;
S2.rMat          = Q.state.rMat;
S2.eMat          = Q.state.eMat;
S2.plotFrequency = 30;
S2.makeGifFlag   = true;
S2.gifFileName   = 'testGif.gif';
S2.bounds        = max(abs(R.rIstar(:)))*1.2*[-1 1 -1 1 -1 1];
visualizeQuad(S2,R);

%% Plot.

plotQuadDynamics(Q,R);
figure(1), sgtitle('')

% Get the body X axis in inertial frame. Loop through all times with a
% decimation factor. Formulate the decimation factor based on the frequency
% of desired plot.
dec_interval = .1;
dec_factor   = floor(dec_interval*S.oversampFact/delt);

% Begin with largest possible matrix, filter out all nan later.
bodyX_i = nan(size(Q.state.eMat));
for i_time=1:dec_factor:length(Q.tVec)
    RBI = euler2dcm(Q.state.eMat(i_time,:)');
    bodyX_i(i_time,:) = RBI' * [1 0 0]';
end
% Take body [X,Y] position at the same times we got the Euler angles.
bodypos = Q.state.rMat(~isnan(bodyX_i));
bodypos = reshape(bodypos, length(bodypos)/3,3);
% Filter out all nan.
bodyX_i = rmmissing(bodyX_i);

figure(20), clf
hold on, grid on
plot(Q.state.rMat(:,1),Q.state.rMat(:,2),'k')
axis equal
quiver(bodypos(:,1), bodypos(:,2), ...
       bodyX_i(:,1), bodyX_i(:,2), .5, 'r')
% title('Trajectory Topview')
legend('Trajectory','Body X Axis')
xlabel('Inertial X (m)')
ylabel('Inertial Y (m)')
xlim(xlim*1.2)

figure(21), clf
grid on, hold on
plot(Q.tVec, Q.state.rMat(:,3))
xlabel('Time (s)')
ylabel('Inertial Z Position (m)')